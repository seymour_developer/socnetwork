<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\Friend;
use app\widgets\Alert;
use app\widgets\LanguageDropdown\LanguageDropdown;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php $friendRequests = Friend::friendRequest(Yii::$app->user->identity['id']);?>
<div class="wrap">
    <?php $lang = '';?>
    <?php if (Yii::$app->language == 'az') { ?>
        <?php $lang = '
        <img src="/web/img/aze.png" alt="">
        <span class="lt">AZE</span>';?>
    <?php } else if(Yii::$app->language == 'en') { ?>
        <?php $lang = '
        <img src="/web/img/eng.png" alt="">
        <span class="lt">EN</span>';?>
    <?php } else if(Yii::$app->language == 'ru') { ?>
        <?php $lang = '
        <img src="/web/img/ru.png" alt="">
        <span class="lt">RU</span>';?>
    <?php } ?>

    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);


    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => [

            ['label' => Yii::$app->user->identity['name'], 'url' => ['profile/index']],
            ['label' => "Friends ". (count($friendRequests) ? "<span style='background: red;' class='badge'>".
                        count($friendRequests)."</span>" : '') ,
                    'encode' => false,
                    'options'=>['id'=>'list-friend-requests']
            ],
            ['label' => Yii::t('app','chat'), 'url' => ''],
            ['label' => Yii::t('app','notifications'), 'url' => ''],
            ['label' => $lang,
                'items' => [
                    ['label' => '<img src="/web/img/aze.png" alt=""> <span class="lt">AZE</span>', 'url' => array_merge(
                        Yii::$app->request->get(),
                        [Yii::$app->controller->route, 'language' => 'az'])
                    ],


                    ['label' => '<img src="/web/img/ru.png" alt=""> <span class="lt">RU</span>', 'url' => array_merge(
                        Yii::$app->request->get(),
                        [Yii::$app->controller->route, 'language' => 'ru'])
                    ],


                    ['label' => '<img src="/web/img/eng.png" alt=""> <span class="lt">EN</span>', 'url' => array_merge(
                        Yii::$app->request->get(),
                        [Yii::$app->controller->route, 'language' => 'en'])
                    ],
                ],

            ],

            Yii::$app->user->isGuest ? /*(
                ['label' => 'Login', 'url' => ['/auth/login']]
            ) */ ''
                : (
                '<li>'
                . Html::beginForm(['/auth/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->name . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
    <div class="col-md-12" style="position: absolute;top: 50px;">
        <div class="col-md-4" id="friend-requests-block" style="position: fixed;z-index: 1;right: 145px;display: none;">
            <div class="panel panel-default">
                <div class="panel-heading">Friend Requests</div>
                <div class="panel-body">
                    <?php
                    if($friendRequests) {

                        foreach ($friendRequests as $request) { ?>
                            <div class="col-md-12 friend-request-block" style="padding-left: 0;">
                                <div class="col-md-6" style="padding-left: 0;">
                                    <img style="width: 40px;height: 40px;"
                                         src="<?= (Yii::getAlias('@web') . '/web/uploads/profile_image/' . ($request['hash_name'] ? $request['hash_name'] : 'no-image.png')); ?>"
                                         alt=""/>
                                    <a href="<?= Url::toRoute(['profile/profile',
                                        'id' => $request['user_id']]); ?>">
                                        <?= $request['name']; ?>
                                    </a>
                                </div>

                                <div class="col-md-6">
                                    <input type="hidden" name="friend-request-id"
                                           value="<?= $request['friend_request_id']; ?>"/>
                                    <input type="hidden" name="friend-request-user-id"
                                           value="<?= $request['user_id']; ?>"/>
                                    <button class="btn btn-default" id="friend-request-confirm" type="submit">Confirm
                                    </button>
                                    <button class="btn btn-primary">Delete</button>
                                </div>
                            </div>
                        <?php
                        }
                    } else {?>
                        <p style="text-align:center;">No new requests</p>
                    <?php } ?>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 style="margin-bottom: 20px;border-bottom: 1px solid gray;">You may know them</h5>
                            <?php
                            $friendModel = new Friend();
                            $friends = $friendModel->getAllFriends(Yii::$app->user->identity['id']);
                            if(!empty($friends)){

                                $friendIds = [];
                                foreach ($friends as $item) {
                                    $friendIds[] = (int)$item['id'];
                                }

                                $friendsOfFriend = $friendModel->friendsOfFriend(Yii::$app->user->identity['id'],$friendIds);?>
                                <?php foreach ($friendsOfFriend as $k =>  $data) {?>
                                    <div class="col-md-12 friend-request-block" style="padding-left: 0;padding-right: 0;">
                                        <div class="col-md-4" style="padding-left: 0;">
                                            <img style="width: 40px;height: 40px;border-radius:50%;" src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($data['hash_name'] ? $data['hash_name'] : 'no-image.png'));?>"
                                                 alt="" />
                                            <a href="<?=Url::toRoute(['/profile',
                                                'id' => $data['id']]); ?>">
                                                <?=$data['name'];?>
                                            </a>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="hidden" name="friend-id" value="<?=$data['id'];?>"/>
                                            <input type="hidden" name="friendship-status" value="<?=$friendModel->getFriendshipStatusForUser(Yii::$app->user->identity['id'],$data['id']);?>">
                                            <button class="btn btn-default" id="add-friend" type="submit">
                                                <?=$friendModel->getFriendshipStatusForUser(Yii::$app->user->identity['id'],$data['id']);?>
                                            </button>
                                            <button class="btn btn-primary" style="float: right;">
                                                Remove
                                            </button>
                                        </div>
                                    </div>
                                <?php }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered()?></p>

    </div>
</footer>

<script>
    $(document).ready(function(){
        $("#list-friend-requests").on('click',function(){
            $("#friend-requests-block").toggle();
            return false;
        });


        $("#friend-request-confirm").on('click',function(){
            var friend_request_id = $("input[name=friend-request-id]").val();
            var friend_request_user_id = $("input[name=friend-request-user-id]").val();
            var url = "<?=Yii::getAlias('@web');?>/ajax/accept-friend-request/?friend_request_id="+encodeURIComponent(friend_request_id)+
                    '&friend_request_user_id='+friend_request_user_id+'&logged_user_id='+ <?=Yii::$app->user->identity['id'];?> ;
            $.ajax({
                url: url,
                type: "GET",
                async: true,
                cache: false,
                dataType: 'json',
                success: function(response){
                    if(response.success){
                        $(".friend-request-block").hide();
                    }
                },
                error: function(){
                    console.log("error");
                }
            });
        });

        $("#add-friend").on('click',function(){

            var friendshipStatus = $("input[name=friendship-status]").val();
            if(friendshipStatus!== 'Add friend'){
                return false;
            }
            var friend_id = $("input[name=friend-id]").val();
            var url = "<?=Yii::getAlias('@web');?>/ajax/add-friend/?friend_id="+encodeURIComponent(friend_id)+
                '&user_id='+ <?=Yii::$app->user->identity['id'];?> ;
            $.ajax({
                url: url,
                type: "GET",
                async: true,
                cache: false,
                dataType: 'json',
                success: function(response){
                    if(response.success){
                        $("#add-friend").text("Friend request sent");
                    }
                },
                error: function(){
                    console.log("error");
                }
            });
        });
    });
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
