<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

$this->title = 'My Yii Application';
?>

  <div class="row">
      <div class="col-md-9">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>
            <?php $_SESSION["csrf_token"] = md5(rand(0,10000000)).time();?>
            <input type="hidden" name="csrf_token"
               value="<?=htmlspecialchars($_SESSION["csrf_token"]);?>"/>

            <!-- cover and profile images block starts -->
            <div class="cover-image-block">
                <a href="#">
                    <img  id="myImg"  class="cover-image-block__image"
                          src="<?=(Yii::getAlias('@web').'/web/uploads/cover_image/'.($current_cover_image ? $current_cover_image : 'no-image.png'));?>" alt=""/>
                </a>
                <div id="myModal" class="modal">
                    <span class="close">&times;</span>
                    <img class="modal-content" id="img01">
                    <div id="caption"></div>
                </div>
                <script>
                    // Get the modal
                    var modal = document.getElementById('myModal');

                    // Get the image and insert it inside the modal - use its "alt" text as a caption
                    var img = document.getElementById('myImg');
                    var modalImg = document.getElementById("img01");
                    var captionText = document.getElementById("caption");
                    img.onclick = function(){
                        modal.style.display = "block";
                        modalImg.src = this.src;
                        captionText.innerHTML = this.alt;
                    }

                    // Get the <span> element that closes the modal
                    var span = document.getElementsByClassName("close")[0];

                    // When the user clicks on <span> (x), close the modal
                    span.onclick = function() {
                        modal.style.display = "none";
                    }
                </script>
                <style>
                    #myImg {
                        border-radius: 5px;
                        cursor: pointer;
                        transition: 0.3s;
                    }

                    #myImg:hover {opacity: 0.7;}

                    /* The Modal (background) */
                    .modal {
                        display: none; /* Hidden by default */
                        position: fixed; /* Stay in place */
                        z-index: 1; /* Sit on top */
                        padding-top: 100px; /* Location of the box */
                        left: 0;
                        top: 0;
                        width: 100%; /* Full width */
                        height: 100%; /* Full height */
                        overflow: auto; /* Enable scroll if needed */
                        background-color: rgb(0,0,0); /* Fallback color */
                        background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
                    }

                    /* Modal Content (image) */
                    .modal-content {
                        margin: auto;
                        display: block;
                        width: 80%;
                        max-width: 700px;
                    }

                    /* Caption of Modal Image */
                    #caption {
                        margin: auto;
                        display: block;
                        width: 80%;
                        max-width: 700px;
                        text-align: center;
                        color: #ccc;
                        padding: 10px 0;
                        height: 150px;
                    }

                    /* Add Animation */
                    .modal-content, #caption {
                        -webkit-animation-name: zoom;
                        -webkit-animation-duration: 0.6s;
                        animation-name: zoom;
                        animation-duration: 0.6s;
                    }

                    @-webkit-keyframes zoom {
                        from {-webkit-transform:scale(0)}
                        to {-webkit-transform:scale(1)}
                    }

                    @keyframes zoom {
                        from {transform:scale(0)}
                        to {transform:scale(1)}
                    }

                    /* The Close Button */
                    .close {
                        position: absolute;
                        top: 15px;
                        right: 35px;
                        color: #f1f1f1;
                        font-size: 40px;
                        font-weight: bold;
                        transition: 0.3s;
                    }

                    .close:hover,
                    .close:focus {
                        color: #bbb;
                        text-decoration: none;
                        cursor: pointer;
                    }

                    /* 100% Image Width on Smaller Screens */
                    @media only screen and (max-width: 700px){
                        .modal-content {
                            width: 100%;
                        }
                    }
                </style>


                <div class="col-md-12" style="padding: 10px;position: absolute;top: 0;">
                    <!-- Block to config cover image -->
                    <div class="col-md-4 config-cover-image">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="fa fa-camera-retro"></i> Update cover image
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li>
                                    <a class="config-cover-image__dropdown-items__select-item">
                                        Select image
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <?= $form->field($userPhotoModel, 'cover_image', [
                                        ])->fileInput([
                                                'onchange' => 'uploadCover(this)']
                                        )->label('Upload') ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=Url::toRoute(['profile/remove-cover-image','hash_name' => $current_cover_image]);?>" class="col-md-12 config-cover-image__remove">
                                        <label for="">Remove</label>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Block to config cover image -->

                    <!-- Block to select cover image from existing cover images -->
                    <div class="cover-image-block__select panel panel-default" style="display:none;">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <h5><b>Select Cover Image</b></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="#" class="cover-image-block__select__close correct_close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12 cover-image-block__select__images" style="padding-left: 0;padding-right: 0;">
                                <?php
                                $byType = [];
                                foreach ($all_photos as $k => $data) {
                                    $byType[$data['type']][] = $data;
                                }
                                ?>
                                <?php foreach ($byType as $k => $images) {?>
                                    <div class="col-md-12">
                                        <?php
                                        if($k == 'cover_image') {
                                            foreach ($images as $image) { ?>
                                                <div class="col-md-3">
                                                    <input class="profile-image-block__input_hidden" type='radio'
                                                           value="<?= $image['id']; ?>" name='chooseCoverImage'
                                                           id="coverImageRadio<?= $image['id']; ?>"/>
                                                    <label for="coverImageRadio<?= $image['id']; ?>">
                                                        <a type="button" class="btn btn-default">
                                                            <img style="height: 112px;width: 112px;"
                                                                 src="<?= Yii::getAlias('@web') . '/web/uploads/' . $k . '/' . $image['hash_name']; ?>"
                                                                 alt="">
                                                        </a>
                                                    </label>
                                                </div>
                                            <?php }
                                        }
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="Use this image" class="btn btn-primary"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Block to select cover image from existing cover images -->
                </div>

                <!-- Profile image block starts -->
                <div class="col-md-12" style="position: absolute;top: 130px;">
                    <div class="col-md-3 profile-image">
                        <div style="height: 168px;width:168px;">
                            <img src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($current_profile_image ? $current_profile_image : 'no-image.png'));?>" alt="" style="
                                    max-height: 100%;width:100%;
                                    border-radius: 5px 20px 5px;border: 2px solid white;
                                        ">
                            <div class="col-md-12" style="bottom: 46px;height: 45px;background: #504a4a;">
                                <a class='btn btn-default' id="profile_image_box" >Upload Image</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 profile-name" style="padding-left: 0;">
                        <h1 style="color: white;">
                            <a style="text-decoration: none;color:white;"
                               href="<?= Url::toRoute(['/'.Yii::$app->user->identity['name']]);?>"><?=Yii::$app->user->identity['name'];?>
                            </a>
                        </h1>
                    </div>
                    <div class="col-12 cover-image-upload-submit" style="margin-top: 140px;text-align: right;display:none;">
                        <input type="button" class="btn btn-default" value="Cancel" onClick="window.location.reload()">
                        <input  type="submit" class="btn btn-success" value="Save"/>
                    </div>

                    <!-- Block profile image block -->
                    <div class="profile-image-block panel panel-default" style="display:none;">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <h5><b>Update Profile Image</b></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="#" class="profile-image-block__close correct_close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="col-md-6 profile-image-block__upload">
                                <?= $form->field($userPhotoModel, 'profile_image', [
                                ])->fileInput(['onchange' => 'imagepreview(this)'])->label();
                                ?>
                            </div>
                            <div class="col-md-12 profile-image-block__images" style="padding-left: 0;padding-right: 0;">
                                <?php
                                $byType = [];
                                foreach ($all_photos as $k => $data) {
                                    $byType[$data['type']][] = $data;
                                }
                                ?>
                                <?php foreach ($byType as $k => $images) {?>
                                    <div class="col-md-12" style="padding-left: 0;">
                                        <div class="col-md-12">
                                            <h4><?=Yii::t('app','category_'.$k);?></h4>
                                        </div>
                                        <br/>
                                        <?php foreach ($images as $image) {?>
                                            <div class="col-md-3">
                                                <input class="profile-image-block__input_hidden" type='radio' value="<?=$image['id'];?>" name='chooseProfileImage' id="radio<?=$image['id'];?>"/>
                                                <label for="radio<?=$image['id'];?>">
                                                    <a type="button" class="btn btn-default">
                                                        <img style="height: 112px;width: 112px;" src="<?=Yii::getAlias('@web').'/web/uploads/'.$k.'/'.$image['hash_name'];?>" alt="">
                                                    </a>
                                                </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="Use this image" class="btn btn-primary"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Block profile image block -->


                    <!-- Block to upload new profile image -->
                    <div class="block-create-profile-image" style="display: none;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>Create profile picture</b>
                            </div>
                            <div class="panel-body">
                                <div class="col-12" style="text-align: center;">
                                    <img src="" id="output" style="width: 200px;height: 180px;" alt="">
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6" style="padding-right: 0;float: right;text-align:right;">
                                            <input type="button" class="btn btn-default" value="Cancel" onClick="window.location.reload()">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Block to upload new profile image -->
                </div>
                <!-- Profile image block ends -->
            </div>
            <!-- cover and profile images block ends -->
        <?php $form->end();?>

        <!-- Information fields -->
        <div class="col-md-12" style="padding-left: 0;padding-right: 0;margin-top: 15px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <i class="fa fa-user"></i> About
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="col-md-12" style="padding-left: 0;padding-right: 0;">

                        <div id="exTab1">
                            <!-- Tab Titles -->
                            <div class="col-md-4" style="border-right: 1px solid gray;padding-left: 0;">
                                <ul class="nav nav-pills">
                                    <li class="active">
                                        <a href="#workAndEducation" data-toggle="tab">Work and education</a>
                                    </li>
                                    <li>
                                        <a href="#lived_places" data-toggle="tab">Placed you have lived</a>
                                    </li>
                                    <li>
                                        <a href="#relationship_status" data-toggle="tab">Family and relationships</a>
                                    </li>
                                    <li>
                                        <a href="#add_note" data-toggle="tab">Details about you</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- Tab Titles -->

                            <!-- Tab content -->
                            <div class="col-md-8" style="padding-right: 0;">
                                <div class="tab-content clearfix">

                                    <!-- Personal information and etc -->
                                    <div class="tab-pane active" id="workAndEducation">
                                        <div class="panel panel-info" style="text-align: center;">
                                            <div class="panel-heading">
                                                <div class="row"></div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">

                                                    <!-- adds work places starts-->
                                                    <div class="col-md-12" style="padding-left: 0;padding-right: 0;margin-bottom: 50px;">
                                                        <div class="col-md-12" style="margin-bottom:10px;">
                                                            <div class="col-md-12" style="border-bottom:1px solid gray;padding-left:0;">
                                                                <h3 style="float:left;padding-bottom:10px;"
                                                                    class="panel-title"> Workplace
                                                                </h3>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-4 add-work-place-button-block"
                                                                 style="border-style: dotted;padding: 10px;text-align: center;border-width: 2px;
                                                                    border-color:blue;cursor: pointer;background: #f6f7f9;">
                                                                <span class="add-work-place">
                                                                    <i class="fa fa-plus"></i> Add workplace
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <?php $workPlaceForm = ActiveForm::begin();?>
                                                        <div class="col-md-12 add-work-place-block" style="display:none;">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-5" style="float: right;padding-right: 0;text-align: right;">
                                                                        <span style="cursor: pointer;" class="add-work-cancel-button">
                                                                            <p>Cancel
                                                                                <i class="fa fa-times"></i>
                                                                            </p>
                                                                    </span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="col-md-6">
                                                                        <?=$workPlaceForm->field($workPlaceModel,'company')
                                                                            ->textInput()
                                                                            ->label('Company');
                                                                        ?>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <?=$workPlaceForm->field($workPlaceModel,'position')
                                                                            ->textInput()
                                                                            ->label('Position');
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="col-md-5">
                                                                        <?=$workPlaceForm->field($workPlaceModel,'city')
                                                                            ->textInput()
                                                                            ->label('City');
                                                                        ?>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="col-md-6">
                                                                        <div class="checkbox" style="text-align: left;">
                                                                            <label>
                                                                                <input id="toPresent" name="currently_work" type="checkbox" value="1" checked>I currently work here
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="col-md-6">
                                                                        <?php
                                                                        echo $workPlaceForm->field($workPlaceModel, 'start_date')->widget(DatePicker::classname(), [
                                                                            'options' => ['placeholder' => 'Başlama tarixi',
                                                                            ],
                                                                            'pluginOptions' => [
                                                                                'autoclose' => true,
                                                                                'format' => 'yyyy/mm/dd'
                                                                            ]
                                                                        ]);
                                                                        ?>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="over_date" style="display: none;">
                                                                            <?php
                                                                            echo $workPlaceForm->field($workPlaceModel, 'over_date')->widget(DatePicker::classname(), [
                                                                                'options' => ['placeholder' => 'Bitmə tarixi',
                                                                                ],
                                                                                'pluginOptions' => [
                                                                                    'autoclose' => true,
                                                                                    'format' => 'yyyy/mm/dd'
                                                                                ]
                                                                            ]);
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12" style="margin-top: 30px;">
                                                                    <input type="submit" class="btn btn-primary" value="Save"/>
                                                                    <a class="btn btn-default add-work-cancel-button">Cancel</a>
                                                                </div>
                                                        </div>
                                                        <?php $workPlaceForm->end();?>

                                                        <div class="col-md-12 all-work-places" style="margin-top: 30px;">
                                                            <?php foreach ($allWorkPlaces as $workPlace) {?>
                                                                <div class="col-md-12" style="border-top: 1px solid gray;text-align: left;padding-right:0;padding-left: 0;">
                                                                    <div class="col-md-12" style="padding-left: 0;padding-right:0;">
                                                                        <div class="col-md-12" style="padding-left: 0;padding-right:0;">
                                                                            <div class="col-md-6" style="padding-left: 0;">
                                                                                <h4><?=$workPlace['company'];?></h4>
                                                                            </div>
                                                                            <div class="col-md-6" style="float: right;text-align: right;padding-top: 10px;padding-right:0;">
                                                                                <i style="cursor: pointer;" class="fa fa-edit"></i>
                                                                                <a style="cursor: pointer;" data-workplace-id="<?=$workPlace['id'];?>" class="remove-workplace">
                                                                                    <i class="fa fa-times"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <p class="position"><?=$workPlace['position'];?> <?=$workPlace['city'];?>
                                                                            <?=($workPlace['currently_work'] ? $workPlace['start_date'] .' to present' : '')?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <!-- adds work places ends-->


                                                    <!-- adds university block starts -->
                                                    <div class="col-md-12" style="padding-left: 0;padding-right: 0;margin-bottom: 50px;">
                                                        <div class="col-md-12" style="margin-bottom:10px;">
                                                            <div class="col-md-12" style="border-bottom:1px solid gray;padding-left:0;">
                                                                <h3 style="float:left;padding-bottom:10px;"
                                                                    class="panel-title"> Add university
                                                                </h3>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-4 add-university-button-block"
                                                                 style="border-style: dotted;padding: 10px;text-align: center;border-width: 2px;
                                                                    border-color:blue;cursor: pointer;">
                                                                 <i class="fa fa-plus"></i> University
                                                            </div>
                                                        </div>
                                                        <?php $universityActiveForm = ActiveForm::begin();?>
                                                        <div class="col-md-12 add-university-block" style="display: none;background: #f6f7f9;">
                                                            <div class="col-md-12">
                                                                <div class="col-md-5" style="float: right;padding-right: 0;text-align: right;">
                                                                    <span style="cursor: pointer;" class="user-university-cancel-button">
                                                                        <p>Cancel
                                                                            <i class="fa fa-times"></i>
                                                                        </p>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="col-md-6">
                                                                    <?php
                                                                        echo $universityActiveForm->field($userUniversityModel, 'university_code')
                                                                        ->dropDownList($universityList,['prompt' => Yii::t('app', 'Select')]);
                                                                    ?>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="col-md-6" style="text-align: left;">
                                                                    <?= $universityActiveForm->field($userUniversityModel, 'is_graduated')->checkbox(array('label'=>''))
                                                                        ->label('Graduated'); ?>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="col-md-6">
                                                                    <?php
                                                                    echo $universityActiveForm->field($userUniversityModel, 'study_start_date')->widget(DatePicker::classname(), [
                                                                        'options' => ['placeholder' => 'Başlama tarixi',
                                                                        ],
                                                                        'pluginOptions' => [
                                                                            'autoclose' => true,
                                                                            'format' => 'yyyy/mm/dd'
                                                                        ]
                                                                    ]);
                                                                    ?>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="over_date">
                                                                        <?php
                                                                        echo $universityActiveForm->field($userUniversityModel, 'study_over_date')->widget(DatePicker::classname(), [
                                                                            'options' => ['placeholder' => 'Bitmə tarixi',
                                                                            ],
                                                                            'pluginOptions' => [
                                                                                'autoclose' => true,
                                                                                'format' => 'yyyy/mm/dd'
                                                                            ]
                                                                        ]);
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12" style="margin-top: 30px;">
                                                                <input type="submit" class="btn btn-primary" value="Save"/>
                                                                <button class="btn btn-default user-university-cancel-button">Cancel</button>
                                                            </div>
                                                        </div>
                                                        <?php $universityActiveForm->end();?>

                                                        <div class="col-md-12 all-universities" style="margin-top: 30px;">
                                                            <?php foreach ($allUniversities as $university) {?>
                                                                <div class="col-md-12" style="border-top: 1px solid gray;text-align: left;">
                                                                    <div class="col-md-12" style="padding-left: 0;">
                                                                        <h4><?=$university['name'];?></h4>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <!-- adds university block ends -->

                                                    <!-- add profession skills block starts -->
                                                    <div class="col-md-12" style="padding-left: 0;padding-right: 0;margin-bottom: 50px;">
                                                        <div class="col-md-12" style="margin-bottom:10px;">
                                                            <div class="col-md-12" style="border-bottom:1px solid gray;padding-left:0;">
                                                                <h3 style="float:left;padding-bottom:10px;"
                                                                    class="panel-title"> Professional skills
                                                                </h3>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-5 add-prof-skill-button-block"
                                                                 style="border-style: dotted;padding: 10px;text-align: center;border-width: 2px;
                                                                    border-color:blue;cursor: pointer;">
                                                                <span class="add-work-place">
                                                                    <i class="fa fa-plus"></i> Add Professional skill
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 add-skill-block" style="display:none;background: #f6f7f9;padding:10px;">
                                                            <div class="col-md-12">
                                                                <div class="col-md-5" style="float: right;padding-right: 0;text-align: right;">
                                                                    <span style="cursor: pointer;" class="add-skill-cancel-button">
                                                                        <p>Cancel
                                                                            <i class="fa fa-times"></i>
                                                                        </p>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <script>
                                                                $(document).ready(function(){
                                                                     var maxFields = 10;
                                                                     var addButton = $(".add_button");
                                                                     var wrapper = $(".field_wrapper");
                                                                     var fieldHtml =
                                                                        '<div style="margin-bottom:10px;">' +
                                                                             '<div class="col-md-10">'+
                                                                                '<input type="text" name="skills[]" class="form-control" value=""/>' +
                                                                             '</div>'+
                                                                             '<div class="col-md-1">'+
                                                                                '<a href="javascript:void(0);" class="remove_button" title="Remove field">' +
                                                                                '<i class="fa fa-minus"></i></a>' +
                                                                             '</div>'+
                                                                         '</div>';
                                                                    var x = 1;
                                                                     $(addButton).on('click',function(){
                                                                         if(x < maxFields){
                                                                             x++;
                                                                             $(wrapper).append(fieldHtml);
                                                                         }
                                                                     });

                                                                     $(wrapper).on('click','.remove_button',function(e){
                                                                        e.preventDefault();
                                                                        $(this).parent().parent().remove();
                                                                        x--;
                                                                     });
                                                                });
                                                            </script>
                                                            <?php $profSkillForm = ActiveForm::begin();?>
                                                                <div class="col-md-6" style="margin:0 auto;float: none;">
                                                                    <div class="field_wrapper">
                                                                        <div style="margin-bottom: 10px;">
                                                                            <div class="col-md-10">
                                                                                <input type="text" name="skills[]" class="form-control" value=""/>
                                                                            </div>
                                                                            <div class="col-md-1" style="padding-top: 4px;">
                                                                                <a href="javascript:void(0);" class="add_button" title="Add field">
                                                                                    <i class="fa fa-plus"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12" style="margin-top: 30px;">
                                                                    <input type="submit" class="btn btn-primary" value="Save"/>
                                                                    <button class="btn btn-default add-skill-cancel-button">Cancel</button>
                                                                </div>
                                                            <?php $profSkillForm->end();?>
                                                        </div>
                                                        <div class="col-md-12 all-skills" style="margin-top: 30px;">
                                                            <?php foreach ($allSkills as $skill) {?>
                                                                <div class="col-md-2">
                                                                    <p style="inline-block;"><b><?=$skill?></b></p>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <!-- add profession skills block ends-->


                                                    <!-- adds high school block starts -->
                                                    <div class="col-md-12" style="padding-left: 0;padding-right: 0;margin-bottom: 50px;">
                                                        <div class="col-md-12" style="margin-bottom:10px;">
                                                            <div class="col-md-12" style="border-bottom:1px solid gray;padding-left:0;">
                                                                <h3 style="float:left;padding-bottom:10px;"
                                                                    class="panel-title"> Add school
                                                                </h3>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-4 add-school-button-block"
                                                                 style="border-style: dotted;padding: 10px;text-align: center;border-width: 2px;
                                                                    border-color:blue;cursor: pointer;">
                                                                <i class="fa fa-plus"></i> School
                                                            </div>
                                                        </div>

                                                        <?php $schoolActiveForm = ActiveForm::begin();?>
                                                        <div class="col-md-12 add-school-block" style="display: none;background: #f6f7f9;padding: 10px;">
                                                            <div class="col-md-12">
                                                                <div class="col-md-5" style="float: right;padding-right: 0;text-align: right;">
                                                                    <span style="cursor: pointer;" class="user-school-cancel-button">
                                                                        <p>Cancel
                                                                            <i class="fa fa-times"></i>
                                                                        </p>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="col-md-6">
                                                                    <?php
                                                                    echo $schoolActiveForm->field($userSchoolModel, 'school_id')
                                                                        ->dropDownList($schools,['prompt' => Yii::t('app', 'Select')]);
                                                                    ?>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="col-md-6" style="text-align: left;">
                                                                    <?= $schoolActiveForm->field($userSchoolModel, 'is_graduated')->checkbox(array('label'=>''))
                                                                        ->label('Graduated'); ?>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="col-md-6">
                                                                    <?php
                                                                    echo $schoolActiveForm->field($userSchoolModel, 'study_start_date')->widget(DatePicker::classname(), [
                                                                        'options' => ['placeholder' => 'Başlama tarixi',
                                                                        ],
                                                                        'pluginOptions' => [
                                                                            'autoclose' => true,
                                                                            'format' => 'yyyy/mm/dd'
                                                                        ]
                                                                    ]);
                                                                    ?>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="over_date">
                                                                        <?php
                                                                        echo $schoolActiveForm->field($userSchoolModel, 'study_over_date')->widget(DatePicker::classname(), [
                                                                            'options' => ['placeholder' => 'Bitmə tarixi',
                                                                            ],
                                                                            'pluginOptions' => [
                                                                                'autoclose' => true,
                                                                                'format' => 'yyyy/mm/dd'
                                                                            ]
                                                                        ]);
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12" style="margin-top: 30px;">
                                                                <input type="submit" class="btn btn-primary" value="Save"/>
                                                                <button class="btn btn-default user-school-cancel-button">Cancel</button>
                                                            </div>
                                                        </div>
                                                        <?php $schoolActiveForm->end();?>

                                                        <div class="col-md-12 all-universities" style="margin-top: 30px;">
                                                            <?php foreach ($allUserSchools as $school) {?>
                                                                <div class="col-md-12" style="border-top: 1px solid gray;text-align: left;">
                                                                    <div class="col-md-12" style="padding-left: 0;">
                                                                        <h4><?=$school['name'];?></h4>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <!-- adds high school block ends -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Personal information and etc -->


                                    <!-- Places you have lived tab starts -->
                                    <div class="tab-pane" id="lived_places">
                                        <div class="col-md-12" style="padding-left: 0;padding-right: 0;margin-bottom: 50px;">
                                            <div class="col-md-12" style="margin-bottom:10px;">
                                                <div class="col-md-12" style="border-bottom:1px solid gray;padding-left:0;">
                                                    <h3 style="float:left;padding-bottom:10px;"
                                                        class="panel-title"> Current city and home town
                                                    </h3>
                                                </div>
                                            </div>


                                            <div class="col-md-12">
                                            <?php if(!$currentCity){?>
                                                <div class="col-md-4 add-current-city-button-block"
                                                     style="border-style: dotted;padding: 10px;text-align: center;border-width: 2px;
                                                                    border-color:blue;cursor: pointer;">
                                                    <i class="fa fa-plus"></i> Add current city
                                                </div>
                                            <?php } else {?>

                                                <div class="col-md-12" style="padding-left: 0;">
                                                    <div class="col-md-6" style="padding-left: 0;">
                                                        <h3><?=$currentCity['name'];?></h3>
                                                    </div>
                                                    <div class="col-md-6" style="padding-top: 28px;">
                                                        <a href="<?=Url::toRoute(['profile/remove-city','id'=> $currentCity['id']]);?>">
                                                            <i style="float: right;" class="fa fa-times"></i>
                                                        </a>

                                                    </div>
                                                </div>
                                            <?php } ?>
                                            </div>

                                            <?php $currentCityForm = ActiveForm::begin();?>
                                            <div class="col-md-12 add-current-city-block" style="display: none;background: #f6f7f9;padding: 10px;">
                                                <div class="col-md-12">
                                                    <div class="col-md-5" style="float: right;padding-right: 0;text-align: right;">
                                                        <span style="cursor: pointer;" class="user-current-city-cancel-button">
                                                            <p>Cancel
                                                                <i class="fa fa-times"></i>
                                                            </p>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-6" style="text-align: left;">
                                                        <?php
                                                        echo $currentCityForm->field($userCityModel, 'city_id')
                                                            ->dropDownList($cities,['prompt' => Yii::t('app', 'Select ' )]);
                                                        ?>
                                                        <input type="hidden" name="current_city" value="1">
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="margin-top: 30px;">
                                                    <input type="submit" class="btn btn-primary" value="Save"/>
                                                    <button class="btn btn-default user-current-city-cancel-button">Cancel</button>
                                                </div>
                                            </div>
                                            <?php $currentCityForm->end();?>


                                            <div class="col-md-12" style="margin-top: 10px;">
                                                <?php if(!$homeTown) {?>
                                                <div class="col-md-4 add-home-town-button-block"
                                                     style="border-style: dotted;padding: 10px;text-align: center;border-width: 2px;
                                                                    border-color:blue;cursor: pointer;">
                                                    <i class="fa fa-plus"></i> Add home town
                                                </div>
                                                <?php } else { ?>
                                                    <div class="col-md-12" style="padding-left: 0;">
                                                        <div class="col-md-6" style="padding-left: 0;">
                                                            <h3><?=$homeTown['name'];?></h3>
                                                        </div>
                                                        <div class="col-md-6" style="padding-top: 28px;">
                                                            <a href="<?=Url::toRoute(['profile/remove-city','id'=> $homeTown['id']]);?>">
                                                                <i style="float: right;" class="fa fa-times"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>

                                            <?php $homeTownForm = ActiveForm::begin();?>
                                            <div class="col-md-12 add-home-town-block" style="display: none;background: #f6f7f9;padding: 10px;">
                                                <div class="col-md-12">
                                                    <div class="col-md-5" style="float: right;padding-right: 0;text-align: right;">
                                                        <span style="cursor: pointer;" class="user-home-town-cancel-button">
                                                            <p>Cancel
                                                                <i class="fa fa-times"></i>
                                                            </p>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-6" style="text-align: left;">
                                                        <?php
                                                        echo $homeTownForm->field($userCityModel, 'city_id')
                                                            ->dropDownList($cities,['prompt' => Yii::t('app', 'Select' )]);
                                                        ?>
                                                        <input type="hidden" name="home_town" value="1">
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="margin-top: 30px;">
                                                    <input type="submit" class="btn btn-primary" value="Save"/>
                                                    <button class="btn btn-default user-home-town-button">Cancel</button>
                                                </div>
                                            </div>
                                            <?php $homeTownForm->end();?>
                                        </div>
                                    </div>
                                    <!-- Places you have lived tab ends -->



                                    <!--Relationship status and family members starts-->
                                    <div class="tab-pane" id="relationship_status">
                                        <div class="col-md-12" style="padding-left: 0;padding-right: 0;margin-bottom: 50px;">

                                            <!-- Relation status starts -->
                                            <div class="col-md-12" style="padding-left: 0;">
                                                <div class="col-md-12" style="margin-bottom:10px;">
                                                    <div class="col-md-12" style="border-bottom:1px solid gray;padding-left:0;">
                                                        <h3 style="float:left;padding-bottom:10px;"
                                                            class="panel-title"> Relationship status
                                                        </h3>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">



                                                    <?php if(!$getCurRelStatus){?>
                                                    <div class="col-md-5 add-relationship-status-button-block"
                                                         style="border-style: dotted;padding: 10px;text-align: center;border-width: 2px;
                                                                    border-color:blue;cursor: pointer;">
                                                        <i class="fa fa-plus"></i> Add relationship status
                                                    </div>
                                                    <?php } ?>

                                                    <div style="padding-left: 0;" class="col-md-5 current-status">
                                                        <h3><?=$getCurRelStatus['name'];?></h3>
                                                    </div>

                                                </div>
                                                <div class="col-md-12 add-relationship-status-block"
                                                     style="display: none;background: #f6f7f9;padding: 10px;">
                                                    <div class="col-md-12">
                                                        <div class="col-md-5" style="float: right;padding-right: 0;text-align: right;">
                                                            <span style="cursor: pointer;" class="relationship-status-cancel-button">
                                                                <p>Cancel
                                                                    <i class="fa fa-times"></i>
                                                                </p>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="col-md-6" style="text-align: left;padding-left: 0;">
                                                            <label for="relation_status">Relation status</label>
                                                            <select name="relation_status" class="form-control">
                                                                <option value="">Select</option>
                                                                <option value="1">Single</option>
                                                                <option value="2">In relationship</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12" style="margin-top: 30px;">
                                                        <a type="button" class="btn btn-primary" id="saveRelationshipStatus">Save</a>
                                                        <button class="btn btn-default relationship-status-cancel-button">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Relation status ends -->


                                            <!-- Family members block starts -->
                                            <div class="col-md-12" style="margin-top: 10px;">
                                                <!-- Relation status starts -->
                                                <div class="col-md-12" style="margin-bottom:10px;padding-left: 0;">
                                                    <div class="col-md-12" style="border-bottom:1px solid gray;padding-left:0;">
                                                        <h3 style="float:left;padding-bottom:10px;"
                                                            class="panel-title"> Family members
                                                        </h3>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 add-family-member-button-block"
                                                     style="border-style: dotted;padding: 10px;text-align: center;border-width: 2px;
                                                                border-color:blue;cursor: pointer;">
                                                    <i class="fa fa-plus"></i> Add family member
                                                </div>

                                                <div class="col-md-12" style="padding-left: 0;">
                                                    <div class="col-md-6" style="padding-left: 0;">
                                                        <h3></h3>
                                                    </div>
                                                    <div class="col-md-6" style="padding-top: 28px;"></div>
                                                </div>
                                            </div>

                                            <!-- Family members block starts -->
                                            <div class="col-md-12 add-family-member-block"
                                                 style="display: none;background: #f6f7f9;
                                                padding: 10px;">
                                                <div class="col-md-12">
                                                    <div class="col-md-5" style="float: right;padding-right: 0;text-align: right;">
                                                        <span style="cursor: pointer;" class="add-family-member-cancel-button">
                                                            <p>Cancel
                                                                <i class="fa fa-times"></i>
                                                            </p>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-6" style="padding-left: 0;">
                                                        <label for="user-member">Family member</label>
                                                        <input type="text" class="form-control" name="user-family-member"/>
                                                        <input type="hidden" name="hidden-relation-user-id">
                                                        <div id="putHereFoundUsers"></div>
                                                    </div>
                                                    <style>
                                                        #putHereFoundUsers ul li {
                                                            list-style-type: none;
                                                            cursor: pointer;
                                                        }
                                                        #putHereFoundUsers ul {
                                                            padding:5px;
                                                        }
                                                    </style>
                                                    <div class="col-md-6">
                                                        <label for="relation_type">Relation status</label>
                                                        <select name="relation_type" class="form-control">
                                                            <option value="">Select</option>
                                                            <?php foreach ($getRelativeTypes as $type) {?>
                                                            <option value="<?=$type['id'];?>"><?=$type['name'];?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12" style="margin-top: 30px;">
                                                    <a class="btn btn-primary" id="add-family-member">Save</a>
                                                    <button class="btn btn-default add-family-member-cancel-button">Cancel</button>
                                                </div>
                                            </div>
                                            <!-- Family members block ends -->


                                            <!-- Family member list starts -->
                                            <div class="col-md-12 family-member-list">
                                                <?php if($getRelativesList) { ?>
                                                    <?php foreach ($getRelativesList as $relative) { ?>
                                                        <div class="col-md-12" style="padding-left: 0;padding-right: 0;">
                                                            <div class="col-md-6" style="padding-left: 0;">
                                                                <h3><?= $relative['name'];?></h3>
                                                                <p><?= $relative['relative_type']; ?></p>
                                                            </div>
                                                            <div class="col-md-6" style="padding-right: 0;padding-top: 26px;text-align: right;">
                                                                <a class="remove-relative" data-relative-id="<?=$relative['rel_id'];?>">
                                                                    <i class="fa fa-times"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <!-- Family member list starts -->
                                        </div>
                                    </div>
                                    <!--Relationship status and family members ends-->


                                    <!-- Note tab starts -->
                                    <div class="tab-pane" id="add_note">
                                        <div class="col-md-12" style="border: 1px solid #e0e0e0;padding:10px;" >
                                            <div class="block-header" style="background: #c8ffea;">
                                                <span>Qeyd əlavə edin</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Note tab ends -->
                                </div>
                            </div>
                            <!-- Tab content -->
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Information fields -->
  </div>


<style>
    .nav-pills > li {
         float: none;
    }

    #userphotos-cover_image {
        display: none;
    }

    #userphotos-profile_image{
        display: none;
    }


    #userphotos-other_image{
        display: none;
    }

</style>

<style>
    .field-userphotos-cover_image{
        padding-left:10px;
        padding-top:5px;
        cursor: pointer;
    }

    #test:hover .field-userphotos-cover_image {
        background-color: black;
        transition: 0.3s;
        border-radius: 2px;
        border:1px solid white;
    }

    #test:hover .field-userphotos-cover_image {
    #hidden-cover-image-label-text {
        display: block;
    }
</style>


<script>
    $(document).ready(function(){
        $("#profile_image_box").on('click',function(){
            $(".block-profile-image").show();
        });

        $("#close-profile-image-block").on("click",function(){
            $(".block-profile-image").hide();
        });

        $(".add-work-place-button-block").on('click',function(){
            $(this).hide();
            $(".add-work-place-block").show();
            return false;
        });

        $(".add-work-cancel-button").on('click',function(){
            $(".add-work-place-block").hide();
            $(".add-work-place-button-block").show();
            return false;
        });

        $(".add-university-button-block").on('click',function(){
            $(this).hide();
            $(".add-university-block").show();
            return false;

        });

        $(".user-university-cancel-button").on('click',function(){
            $(".add-university-block").hide();
            $(".add-university-button-block").show();
            return false;
        });

        $(".add-prof-skill-button-block").on('click',function(){
            $(this).hide();
            $(".add-skill-block").show();
            return false;
        });

        $(".add-skill-cancel-button").on('click',function(){
            $(".add-skill-block").hide();
            $(".add-prof-skill-button-block").show();
            return false;
        });

        $(".add-school-button-block").on('click',function(){
            $(this).hide();
            $(".add-school-block").show();
            return false;
        });

        $(".user-school-cancel-button").on('click',function(){
            $(".add-school-block").hide();
            $(".add-school-button-block").show();
            return false;
        });


        $(".add-current-city-button-block").on('click',function(){
            $(this).hide();
            $(".add-current-city-block").show();
            return false;
        });

        $(".user-current-city-cancel-button").on('click',function(){
            $(".add-current-city-block").hide();
            $(".add-current-city-button-block").show();
            return false;
        });

        $(".add-home-town-button-block").on('click',function(){
            $(this).hide();
            $(".add-home-town-block").show();
            return false;
        });

        $(".user-home-town-cancel-button").on('click',function(){
            $(".add-home-town-block").hide();
            $(".add-home-town-button-block").show();
            return false;
        });

        $(".add-relationship-status-button-block").on('click',function(){
            $(this).hide();
            $(".add-relationship-status-block").show();
            return false;
        });

        $(".relationship-status-cancel-button").on('click',function(){
            $(".add-relationship-status-block").hide();
            $(".add-relationship-status-button-block").show();
            return false;
        });

        $(".add-family-member-button-block").on('click',function(){
            $(this).hide();
            $(".add-family-member-block").show();
        });

        $(".add-family-member-cancel-button").on('click',function(){
           $(".add-family-member-block").hide();
           $(".add-family-member-button-block").show();
           return false;
        });

        /**
         * Saves relationship status for the user
         **/
        $("#saveRelationshipStatus").on('click',function(){
            var status_id = $("select[name=relation_status]").val();
            var url = "<?=Yii::getAlias('@web');?>/ajax/save-relationship-status/?status_id="+status_id+
                '&user_id='+<?=Yii::$app->user->identity['id'];?>;
            $.ajax({
                url: url,
                type: "GET",
                async: true,
                cache: false,
                dataType: 'json',
                success: function(response){
                    if(response.success){
                        $(".add-relationship-status-block").hide();
                        $(".add-relationship-status-button-block").hide();
                        $(".current-status h3").append(response.data.name);
                    }
                },
                error: function(){
                    console.log("error");
                }
            });
        });

        /**
         *  Gets family member or relative for the input
         **/
        $("input[name=user-family-member]").keyup(function(){
            $("#putHereFoundUsers").empty();
            var member = $(this).val();
            var url = "<?=Yii::getAlias('@web');?>/ajax/get-family-member/?member="+member;
            if (member){
                $.ajax({
                    url: url,
                    type: "GET",
                    async: true,
                    cache: false,
                    dataType: 'json',
                    success: function (response){
                        if (response.success){
                            var list = '<ul>';
                            $.each(response.data, function( key, value ) {
                                list += '<li data-editible=true data-id='+ value.id +'>'+value.name+'</li>';
                            });
                            list +='</ul>';
                            $("#putHereFoundUsers").empty().append(list);
                            $('[data-editible]').on('click',function(){
                                   var userRelationName = $(this).text();
                                   var userRelationId = $(this).data('id');
                                   $("input[name=hidden-relation-user-id]").val(userRelationId);
                                   if($("input[name=user-family-member]").val(userRelationName)){
                                       $("#putHereFoundUsers").empty();
                                   }
                            });
                        }
                    },
                    error: function () {
                        console.log("error");
                    }
                });
            }
        });

        /**
         * Adds relative for the user
         **/
        $("#add-family-member").on('click',function(){
            var relationUserId = $("input[name=hidden-relation-user-id]").val();
            var relationTypeId = $("select[name=relation_type]").val();
            var url = "<?=Yii::getAlias('@web');?>/ajax/add-user-relation/?relationUserId="+relationUserId+
                    '&relationTypeId='+relationTypeId+'&user_id='+<?=Yii::$app->user->identity['id'];?>;
            if (relationUserId){
                $.ajax({
                    url: url,
                    type: "GET",
                    async: true,
                    cache: false,
                    dataType: 'json',
                    success: function (response){
                        if (response.success) {
                            $(".add-family-member-block").hide();
                            $(".add-family-member-button-block").show();
                            $(".family-member-list").empty();
                            $.each(response.data, function (index, value) {
                                var box = '<div class="col-md-12" style="padding-left: 0;padding-right: 0;">';
                                box += '<div class="col-md-6" style="padding-left: 0;">';
                                box += '<h3>' + value.name + '</h3>';
                                box += '<p>' + value.relative_type + '</p>';
                                box += '</div>';
                                box += '<div class="col-md-6" style="padding-right: 0;padding-top: 26px;text-align: right;">';
                                box += '<a style="cursor: pointer;" class="remove-relative" data-relative-id="' + value.rel_id + '">' +
                                    '<i class="fa fa-times"></i>' +
                                    '</a>';
                                box += '</div>';
                                box += '</div>';
                                $(".family-member-list").append(box);
                            });
                        }
                    },
                    error: function () {
                        console.log("error");
                    }
                });
            }
        });

        $('#toPresent').change(function(){
            if(this.checked)
                $('.over_date').fadeOut('slow');
            else
                $('.over_date').fadeIn('slow');
        });

        /**
         *  Removes user workplaces by ajax
         **/
        $(".all-work-places").on('click','.remove-workplace',function(){
            var workPlaceId = $(this).data('workplace-id');
            var url = "<?=Yii::getAlias('@web');?>/ajax/remove-workplace/?id="+workPlaceId+
                    '&user_id='+<?=Yii::$app->user->identity['id'];?>;
            if (workPlaceId){
                $.ajax({
                    url: url,
                    type: "GET",
                    async: true,
                    cache: false,
                    dataType: 'json',
                    success: function (response){
                        if (response.success){
                            $(".all-work-places").empty();
                            $.each(response.data, function( index, value ) {
                                var div = '<div class="col-md-12" style="border-top: 1px solid gray;text-align: left;padding-right:0;padding-left: 0;">'+
                                            '<div class="col-md-12" style="padding-left: 0;padding-right:0;">'+
                                                '<div class="col-md-12" style="padding-left: 0;padding-right:0;">'+
                                                    '<div class="col-md-6" style="padding-left: 0;">'+
                                                        '<h4>'+value.company+'</h4>'+
                                                    '</div>'+
                                                    '<div class="col-md-6" style="float: right;text-align: right;padding-top: 10px;padding-right:0;">'+
                                                        '<i style="cursor: pointer;" class="fa fa-edit"></i>'+
                                                        '<a style="cursor: pointer;" data-workplace-id="'+value.id+'" class="remove-workplace">'+
                                                            '<i class="fa fa-times"></i>'+
                                                        '</a>'+
                                                    '</div>'+
                                                '</div>'+
                                                '<p>'+value.position+'</p>'+
                                            '</div>'+
                                          '</div>';
                                    $(".all-work-places").append(div);
                            });
                        }
                    },
                    error: function () {
                        console.log("error");
                    }
                });
            }
        });

        /**
         * Removes relatives of the user
         */
        $(".family-member-list").on('click','.remove-relative',function(){
             var relativeId = $(this).data('relative-id');
             var url = "<?=Yii::getAlias('@web');?>/ajax/remove-relative/?id="+relativeId+
                '&user_id='+<?=Yii::$app->user->identity['id'];?>;
             if (relativeId){
                $.ajax({
                    url: url,
                    type: "GET",
                    async: true,
                    cache: false,
                    dataType: 'json',
                    success: function (response){
                        if (response.success){
                            $(".add-family-member-block").hide();
                            $(".add-family-member-button-block").show();
                            $(".family-member-list").empty();
                            $.each(response.data, function( index, value ) {
                                var box = '<div class="col-md-12" style="padding-left: 0;padding-right: 0;">';
                                box += '<div class="col-md-6" style="padding-left: 0;">';
                                box += '<h3>'+value.name+'</h3>';
                                box += '<p>'+value.relative_type+'</p>';
                                box += '</div>';
                                box += '<div class="col-md-6" style="padding-right: 0;padding-top: 26px;text-align: right;">';
                                box += '<a style="cursor: pointer;" class="remove-relative" data-relative-id="'+value.rel_id+'">'+
                                            '<i class="fa fa-times"></i>'+
                                        '</a>';
                                box += '</div>';
                                box += '</div>';
                                $(".family-member-list").append(box);
                            });
                        }
                    },
                    error: function () {
                        console.log("error");
                    }
                });
             }
        });
    });
</script>

<script>
    $(document).ready(function(){
        $("#profile_image_box").on('click',function(){
            $(".profile-image-block").show();
        });

        $('.config-cover-image__dropdown-items__select-item').on('click',function(){
            $(".cover-image-block__select").show();
        });

        $(".profile-image-block__close").on("click",function(){
            $(".profile-image-block").hide();
        });

        $(".cover-image-block__select__close").on('click',function(){
            $(".cover-image-block__select").hide();
        });


        $(".config-cover-image__button").on('click',function(){
            $(".config-cover-image__dropdown-items").toggle();
            return false;
        });

        $(".config-cover-image__remove").on('click',function(){
            return confirm('Are you sure to remove ?');
        })
    });

    function imagepreview(input) {
        var file = input.files[0];
        var fileType = file["type"];
        var size = file["size"];
        var ext = ["image/gif", "image/jpeg", "image/png"];

        if (input.files && file) {
            var reader = new FileReader();

            if ($.inArray(fileType, ext) < 0) {
                alert('Допускаемые форматы :JPG/GIF/PNG');

            } else if (size > 2097152) {
                alert('Не больше 2 мб');

            } else {
                $(".block-create-profile-image").show();
                reader.onload = function (e) {
                    $("#output").attr('src', e.target.result);
                };
                reader.readAsDataURL(file);
            }
        }
    }

    function uploadCover(input) {
        var file = input.files[0];
        var fileType = file["type"];
        var size = file["size"];
        var ext = ["image/gif", "image/jpeg", "image/png"];
        if (input.files && file) {
            var reader = new FileReader();
            if ($.inArray(fileType, ext) < 0) {
                alert('Допускаемые форматы :JPG/GIF/PNG');
            } else if (size > 2097152) {
                alert('Не больше 2 мб');
            } else {
                $(".cover-image-upload-submit").show();
                reader.onload = function (e) {
                    $(".cover-image-block__image").attr('src', e.target.result);
                };
                reader.readAsDataURL(file);
            }
        }
    }
</script>


