<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = 'My Yii Application';
?>
<div class="col-md-9">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>
        <?php $_SESSION["csrf_token"] = md5(rand(0,10000000)).time();?>
        <input type="hidden" name="csrf_token" value="<?=
        htmlspecialchars($_SESSION["csrf_token"]);?>"
        />
        <div class=" row cover-image" style="height: 315px;border: 1px solid #d2d1d1;">
            <img style='width: 100%;max-height: 100%;' src="<?=(Yii::getAlias('@web').'/web/uploads/cover_image/'.($current_cover_image ? $current_cover_image : 'no-image.png'));?>" alt=""/>
            <div class="col-md-12" style="position: absolute;top: 130px;">
                <div class="col-md-3 profile-image">
                    <div style="height: 168px;width:168px;">
                        <img src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($current_profile_image ? $current_profile_image : 'no-image.png'));?>" alt="" style="
                        max-height: 100%;width:100%;
                        border-radius: 5px 20px 5px;border: 2px solid white;
                            "/>
                    </div>
                </div>
                <div class="col-md-3 profile-name" style="padding-left: 0;">
                    <h1 style="color: white;">
                        <a style="text-decoration: none;color:white;"
                           href="<?= Url::toRoute(['others/profile','id'=> $user_info['id']]);?>"><?=$user_info['name'];?>
                        </a>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row" style="padding-right: 0;padding-left: 0;margin-top: 30px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-file-image-o"></i> Photos
                </div>
                <div class="panel-body">
                    <?php
                        $byType = [];
                        foreach ($all_photos as $k => $data) {
                           $byType[$data['type']][] = $data;
                        }
                    ?>
                    <?php
                        foreach ($byType as $k => $images) {
                            foreach ($images as $image) {?>
                                <div class="col-md-3" style="padding-left: 0;padding-top: 10px;">
                                    <img style="width:100%;height: 206px;"
                                         src="<?=(Yii::getAlias('@web').'/web/uploads/'.$k.'/'.($image['hash_name'] ? $image['hash_name'] : 'no-image.png'));?>" alt=""/>
                                </div>
                            <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="dropdown show">
            <a class="btn btn-secondary dropdown-toggle" href="https://example.com" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown link
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
            </div>
        </div>
    <?php ActiveForm::end();?>
</div>

<style>
    #userphotos-cover_image {
        display: none;
    }

    #userphotos-profile_image{
        display: none;
    }


    #userphotos-other_image{
        display: none;
    }
</style>
