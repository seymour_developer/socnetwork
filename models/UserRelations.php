<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_relations".
 *
 * @property int $id
 * @property int $user_id
 * @property int $relation_user_id
 * @property int $relation_id
 */
class UserRelations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_relations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'relation_user_id', 'relation_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'relation_user_id' => Yii::t('app', 'Relation User ID'),
            'relation_id' => Yii::t('app', 'Relation ID'),
        ];
    }


    public function addUserRelation($user_id, $relationUserId, $relationTypeId){

        $sql = "INSERT INTO `user_relations`
                (`user_id`,`relation_user_id`,`relation_id`)
                VALUES ('{$user_id}','{$relationUserId}','{$relationTypeId}')";
        return Yii::$app
            ->db
            ->createCommand($sql)
            ->execute();
    }

    public function getUserRelations($user_id){
        $where = $params = [];
        $where[] = "ur.user_id = :user_id";
        $params[':user_id'] = $user_id;
        $sql = 'SELECT 
                ur.id as rel_id,
                u.name as name,
                frt.name as relative_type
                FROM user_relations ur
                LEFT JOIN family_relations_types frt ON ur.relation_id=frt.id
                LEFT JOIN UserTable u ON ur.relation_user_id=u.id
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';
        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryAll();
    }


    public function getRelativeTypes(){
        $sql = 'SELECT *
                FROM family_relations_types
                ';
        return Yii::$app
            ->db
            ->createCommand($sql)
            ->queryAll();
    }

    public function removeRelative($id){
        $where = $params = [];
        $where[] = "id = :id ";
        $params[':id'] = $id;

        $sql = 'DELETE 
                FROM user_relations
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->execute();
    }



}
