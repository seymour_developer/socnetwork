<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_university_rel".
 *
 * @property int $id
 * @property int $user_id
 * @property string $university_code
 * @property string $study_start_date
 * @property string $study_over_date
 * @property string $is_graduated
 */
class UserUniversityRel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_university_rel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['university_code'],'required'],
            [['user_id'], 'integer'],
            [['study_start_date', 'study_over_date'], 'safe'],
            [['is_graduated'], 'string'],
            [['university_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'university_code' => Yii::t('app', 'Universitet'),
            'study_start_date' => Yii::t('app', 'Başlama tarixi'),
            'study_over_date' => Yii::t('app', 'Bitirmə tarixi'),
            'is_graduated' => Yii::t('app', 'Bitirib'),
        ];
    }


    public function saveUniversityForUser($userUniversity){
        $userUniversityModel = new UserUniversityRel();
        $userUniversityModel->user_id = $userUniversity['user_id'];
        $userUniversityModel->university_code = $userUniversity['university_code'];
        $userUniversityModel->study_over_date = $userUniversity['study_over_date'];
        $userUniversityModel->study_start_date = $userUniversity['study_start_date'];
        $userUniversityModel->is_graduated = $userUniversity['is_graduated'];
        $userUniversityModel->save(false);
    }

    public function getAllUserUniversities($user_id){
        $where = $params = [];
        $where[] = "uur.user_id = :user_id ";
        $params['user_id'] = $user_id;

        $sql = 'SELECT ul.* 
                FROM user_university_rel uur
                LEFT JOIN  university_list ul ON ul.code=uur.university_code
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryAll();
    }
}
