<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_school_rel".
 *
 * @property int $id
 * @property int $user_id
 * @property int $school_id
 * @property string $study_start_date
 * @property string $study_over_date
 * @property string $is_graduated
 */
class UserSchoolRel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_school_rel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id'],'required'],
            [['user_id', 'school_id'], 'integer'],
            [['study_start_date', 'study_over_date'], 'safe'],
            [['is_graduated'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'school_id' => Yii::t('app', 'School'),
            'study_start_date' => Yii::t('app', 'Study Start Date'),
            'study_over_date' => Yii::t('app', 'Study Over Date'),
            'is_graduated' => Yii::t('app', 'Is Graduated'),
        ];
    }


    public function saveSchoolForUser($data){
        $userUniversityModel = new UserSchoolRel();
        $userUniversityModel->user_id = $data['user_id'];
        $userUniversityModel->school_id = $data['school_id'];
        $userUniversityModel->study_over_date = $data['study_over_date'];
        $userUniversityModel->study_start_date = $data['study_start_date'];
        $userUniversityModel->is_graduated = $data['is_graduated'];
        $userUniversityModel->save(false);
    }

    public function getUserSchools($user_id){
        $where = $params = [];
        $where[] = "usr.user_id = :user_id ";
        $params['user_id'] = $user_id;

        $sql = 'SELECT sl.name as name
                FROM user_school_rel usr
                LEFT JOIN  school_list sl ON sl.id=usr.school_id
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryAll();
    }
}
