<?php

namespace app\models;
use Yii;
use yii\web\IdentityInterface;

class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return 'UserTable';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
            'photo' => 'Photo',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'email', 'password', 'photo'], 'string', 'max' => 255],
        ];
    }


    /*public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];*/


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        //return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
        return User::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    /*public static function findByUsername($username)
    {*/
        /*foreach (self::$users as $user) {
            if (strcasecmp($user['name'], $username) === 0) {
                return new static($user);
            }
        }

        return null;*/
        //return User::find()->where(['name' => $username])->one();
    //}

    public static function findByUsername($username)
    {
        return User::find()->where(['name'=> $username])->one();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        //return $this->password === $password;
        return ($this->password == $password) ? true : false;
    }


    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function create(){
        return $this->save(false);
    }

    public function allUsers($user_id){
        $where = $params = [];
        $where[] = "u.id != :user_id";
        $params[':user_id'] = $user_id;
        $sql = 'SELECT u.*
                FROM UserTable u
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';
        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryAll();
    }


    public function getUserInfo($user_id){
        $where = $params = [];
        $where[] = "u.id = :user_id";
        $params[':user_id'] = $user_id;
        $sql = 'SELECT u.*
                FROM UserTable u
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';
        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryOne();
    }


    public function findFamilyMember($member){
        $where = $params = [];
        $member = $member.'%';
        $where[] = "name LIKE :member";
        $params[':member'] = $member;

        $sql = 'SELECT *
                FROM UserTable
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        $users = Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryAll();

        $userData = [];
        foreach ($users as $user) {
          $userData[] = [
              'id' => $user['id'],
              'name' => $user['name']
          ];
        }

        return $userData;
    }

}


