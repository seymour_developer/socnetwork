<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "school_list".
 *
 * @property int $id
 * @property string $name
 */
class SchoolList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }


    public function allSchoolList(){
        $list = SchoolList::find()->asArray()->all();
        $schools = [];
        foreach ($list as $k => $v) {
            $schools[$v['id']] = $v['name'];
        }
        return $schools;
    }
}
