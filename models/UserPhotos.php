<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_photos".
 *
 * @property int $id
 * @property string $hash_name
 * @property string $original_name
 * @property string $is_cover
 * @property string $is_profile
 * @property int $user_id
 * @property int $type
 */
class UserPhotos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_photos';
    }

    public $images;
    public $profile_image;
    public $cover_image;
    public $other_image;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_image','cover_image','other_image'],
                'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'hash_name' => Yii::t('app', 'Hash Name'),
            'original_name' => Yii::t('app', 'Original Name'),
            'is_cover' => Yii::t('app', 'Is Cover'),
            'is_profile' => Yii::t('app', 'Is Profile'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    public function uploadFile($user_id){
        if ($this->validate()) {
            /**
             * Avoids resubmission the form
             */
            if($_SESSION["csrf_token"] == Yii::$app->request->post('csrf_token')) {
                if (!empty($this->images)) {
                    foreach ($this->images as $type => $fileArray) {
                        foreach ($fileArray as $k => $file) {
                            $original_name = $file->name;
                            $hash_name = strtolower(md5(uniqid($file->baseName)) . '.' . $file->extension);
                            $file->saveAs(Yii::getAlias('@app') . '/web/uploads/' . $type . '/' . $hash_name);
                            $saved_image = self::saveFile($type, $original_name, $hash_name, $user_id);
                            if($saved_image->type =='profile_image'){
                                $this->setProfileImage($saved_image->id,$user_id);
                            }
                            if($saved_image->type =='cover_image'){
                                $this->setCoverImage($saved_image->id,$user_id);
                            }

                        }
                    }
                }
            }
        }
    }


    public function setProfileImage($imageId,$user_id){
        $sql = "UPDATE user_photos SET is_profile = '1' 
                WHERE id = '{$imageId}'";
        $set_profile =  Yii::$app
            ->db
            ->createCommand($sql)
            ->execute();


        if($set_profile){
            $set_to_zero_rest = "UPDATE user_photos SET is_profile = '0'
                WHERE id!= '{$imageId}' AND user_id= '{$user_id}'";
            return Yii::$app
                ->db
                ->createCommand($set_to_zero_rest)
                ->execute();
        }
    }



    public function setCoverImage($imageId,$user_id){
        $sql = "UPDATE user_photos SET is_cover = '1' 
                WHERE id = '{$imageId}'";
        $set_cover =  Yii::$app
            ->db
            ->createCommand($sql)
            ->execute();


        if($set_cover){
            $set_to_zero_rest = "UPDATE user_photos SET is_cover = '0'
                WHERE id!= '{$imageId}' AND user_id= '{$user_id}'";
            Yii::$app
                ->db
                ->createCommand($set_to_zero_rest)
                ->execute();
        }
    }


    public function saveFile($type,$original_name,$hash_name,$user_id){
        $image = new UserPhotos();
        $image->hash_name = $hash_name;
        $image->original_name = $original_name;
        $image->user_id = $user_id;
        $image->type = $type;
        $image->save();
        return $image;
    }


    public function getProfileImage($user_id){
        $profile_image = UserPhotos::findOne(['user_id' => $user_id,'type' => 'profile_image',
            'is_profile' => '1']);
        return $profile_image;
    }

    public function getCoverImage($user_id){
        $cover_image = UserPhotos::findOne(['user_id' => $user_id,'type' => 'cover_image',
            'is_cover' => '1']);
        return $cover_image;
    }

    public function getUserPhotos($user_id){
        $where = $params = [];
        $where[] = "up.user_id = :user_id ";
        $where[] = "up.type = 'profile_image'";
        $params['user_id'] = $user_id;

        $sql = 'SELECT up.* 
                FROM user_photos up
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                LIMIT 3';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryAll();
    }


    public function getAllPhotos($user_id){
        $where = $params = [];
        $where[] = "up.user_id =:user_id";
        $params['user_id'] = $user_id;

        $sql = 'SELECT up.* 
                FROM user_photos up
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryAll();
    }

    public function removeCoverImage($hash_name){
        $where = $params = [];
        $where[] = "hash_name =:hash_name";
        $where[] = "type ='cover_image'";
        $params[':hash_name'] = $hash_name;

        $sql = 'DELETE 
                FROM user_photos
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->execute();
    }

    public function showPhoto($id){
        $where = $params = [];
        $where[] = "id =:id";
        $params[':id'] = $id;

        $sql = 'SELECT *
                FROM user_photos
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryOne();
    }

}
