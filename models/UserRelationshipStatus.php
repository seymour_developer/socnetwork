<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_relationship_status".
 *
 * @property int $id
 * @property int $user_id
 * @property int $status_id
 */
class UserRelationshipStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_relationship_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'status_id' => Yii::t('app', 'Status ID'),
        ];
    }

    public function saveUserRelationshipStatus($user_id, $status_id){
        $sql = "INSERT INTO  user_relationship_status (`user_id`,`status_id`)
                VALUES ('{$user_id}','{$status_id}')";

        return Yii::$app
            ->db
            ->createCommand($sql)
            ->execute();
    }


    public function getRelationStatus($user_id){
        $where[] = 'urs.user_id = :user_id';
        $params[':user_id'] = $user_id;
        $sql = 'SELECT rl.name as name 
            FROM user_relationship_status urs
            LEFT JOIN relationship_list rl ON urs.status_id=rl.id
            '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
            ';
        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryOne();
    }



}
