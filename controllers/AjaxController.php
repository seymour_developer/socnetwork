<?php

namespace app\controllers;

use app\models\Friend;
use app\models\User;
use app\models\UserPhotos;
use app\models\UserRelations;
use app\models\UserRelationshipStatus;
use app\models\UserWorkPlaces;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;

class AjaxController extends Controller
{
    public function actionAcceptFriendRequest(){

        if (!Yii::$app->request->isAjax) {
            Yii::$app->end();
        }

        Yii::$app->response->format = 'json';
        $response_data = [
            'success' => false,
            'message' => Yii::t('app', 'ajax_id_err')
        ];

        $GET_data = Yii::$app->request->getQueryParams();

        if (empty($GET_data['friend_request_id'])) {
            return $response_data;
        }
        $friendModel = new Friend();
        $data = [];
        if ($GET_data['friend_request_id'] && is_numeric($GET_data['friend_request_id'])){
            $data['friend_request_id'] = $GET_data['friend_request_id'];
        }

        if ($GET_data['friend_request_user_id'] && is_numeric($GET_data['friend_request_user_id'])){
            $data['friend_request_user_id'] = $GET_data['friend_request_user_id'];
        }

        if ($GET_data['logged_user_id'] && is_numeric($GET_data['logged_user_id'])){
            $data['logged_user_id'] = $GET_data['logged_user_id'];
        }

        $accepted = $friendModel->acceptFriendRequest($data);
        if ($accepted){
            $response_data = [
                'success' => true,
                'message' => Yii::t('app', 'ajax_request_accepted'),
                'data'    => $accepted
            ];
        }
        else {

            $response_data['message'] = Yii::t('app', 'ajax_id_err');
        }
        return $response_data;
    }

    public function actionAddFriend(){

        if (!Yii::$app->request->isAjax) {
            Yii::$app->end();
        }

        Yii::$app->response->format = 'json';
        $response_data = [
            'success' => false,
            'message' => Yii::t('app', 'ajax_add_friend_err')
        ];

        $GET_data = Yii::$app->request->getQueryParams();

        if (empty($GET_data['friend_id']) && empty($GET_data['user_id'])) {
            return $response_data;
        }
        $friendModel = new Friend();
        $sentRequest = $friendModel->sendFriendRequest((int)$GET_data['user_id'],(int)$GET_data['friend_id']);
        if ($sentRequest){
            $response_data = [
                'success' => true,
                'message' => Yii::t('app', 'ajax_sent_friend_request'),
            ];
        }
        else {
            $response_data['message'] = Yii::t('app', 'ajax_id_err');
        }
        return $response_data;
    }

    public function actionSaveRelationshipStatus(){

        if (!Yii::$app->request->isAjax) {
            Yii::$app->end();
        }

        Yii::$app->response->format = 'json';
        $response_data = [
            'success' => false,
            'message' => Yii::t('app', 'ajax_save_relationship_status_err')
        ];

        $GET_data = Yii::$app->request->getQueryParams();

        if (empty($GET_data['user_id']) && empty($GET_data['status_id'])) {
            return $response_data;
        }
        $userRelationshipStatusModel = new UserRelationshipStatus();
        $saved = $userRelationshipStatusModel->saveUserRelationshipStatus((int)$GET_data['user_id'],(int)$GET_data['status_id']);
        if ($saved){
            $status = $userRelationshipStatusModel->getRelationStatus($GET_data['user_id']);
            $response_data = [
                'success' => true,
                'message' => Yii::t('app', 'ajax_rel_status_saved'),
                'data' => $status
            ];
        }
        else {
            $response_data['message'] = Yii::t('app', 'ajax_status_not_saved');
        }
        return $response_data;
    }

    public function actionGetFamilyMember(){

        if (!Yii::$app->request->isAjax) {
            Yii::$app->end();
        }

        Yii::$app->response->format = 'json';
        $response_data = [
            'success' => false,
            'message' => Yii::t('app', 'ajax_get_family_member_err')
        ];

        $GET_data = Yii::$app->request->getQueryParams();

        if (empty($GET_data['member'])) {
            return $response_data;
        }
        $userModel = new User();
        $members = $userModel->findFamilyMember($GET_data['member']);
        if ($members){
            $response_data = [
                'success' => true,
                'message' => Yii::t('app', 'ajax_family_member_found'),
                'data' => $members
            ];
        }
        else {
            $response_data['message'] = Yii::t('app', 'ajax_family_member_not_found');
        }
        return $response_data;
    }

    public function actionAddUserRelation(){

        if (!Yii::$app->request->isAjax) {
            Yii::$app->end();
        }

        Yii::$app->response->format = 'json';
        $response_data = [
            'success' => false,
            'message' => Yii::t('app', 'ajax_add_user_relation_err')
        ];

        $GET_data = Yii::$app->request->getQueryParams();

        if (empty($GET_data['relationUserId']) && empty($GET_data['relationTypeId']) && empty($GET_data['user_id'])) {
            return $response_data;
        }
        $userRelationsModel = new UserRelations();
        $added = $userRelationsModel->addUserRelation($GET_data['user_id'], $GET_data['relationUserId'],$GET_data['relationTypeId']);
        if ($added){
            $relatives = $userRelationsModel->getUserRelations($GET_data['user_id']);
            $response_data = [
                'success' => true,
                'message' => Yii::t('app', 'ajax_user_relation_successfully_added'),
                'data' => $relatives
            ];
        }
        else {
            $response_data['message'] = Yii::t('app', 'ajax_user_relation_not_added');
        }
        return $response_data;
    }


    public function actionRemoveWorkplace(){

        if (!Yii::$app->request->isAjax) {
            Yii::$app->end();
        }

        Yii::$app->response->format = 'json';
        $response_data = [
            'success' => false,
            'message' => Yii::t('app', 'ajax_workplace_remove_err')
        ];

        $GET_data = Yii::$app->request->getQueryParams();

        if (empty($GET_data['id']) ) {
            return $response_data;
        }
        $workPlaceModel = new UserWorkPlaces();
        $removed = $workPlaceModel->removeWorkPlace($GET_data['id']);
        if ($removed){
            $list = $workPlaceModel->allWorkPlaces($GET_data['user_id']);
            $response_data = [
                'success' => true,
                'message' => Yii::t('app', 'ajax_workplace_removed_successfully'),
                'data' => $list
            ];
        }
        else {
            $response_data['message'] = Yii::t('app', 'ajax_workplace_not_removed');
        }
        return $response_data;
    }

    public function actionRemoveRelative(){

        if (!Yii::$app->request->isAjax) {
            Yii::$app->end();
        }

        Yii::$app->response->format = 'json';
        $response_data = [
            'success' => false,
            'message' => Yii::t('app', 'ajax_remove_relative_error')
        ];

        $GET_data = Yii::$app->request->getQueryParams();

        if (empty($GET_data['id']) ) {
            return $response_data;
        }
        $userRelationsModel = new UserRelations();
        $removed = $userRelationsModel->removeRelative($GET_data['id']);
        if ($removed){
            $relatives = $userRelationsModel->getUserRelations($GET_data['user_id']);
            $response_data = [
                'success' => true,
                'message' => Yii::t('app', 'ajax_relative_removed_successfully'),
                'data' => $relatives
            ];
        }
        else {
            $response_data['message'] = Yii::t('app', 'ajax_relative_not_removed');
        }
        return $response_data;
    }

    public function actionUnfriend(){

        if (!Yii::$app->request->isAjax) {
            Yii::$app->end();
        }

        Yii::$app->response->format = 'json';
        $response_data = [
            'success' => false,
            'message' => Yii::t('app', 'ajax_unfriend_error')
        ];

        $GET_data = Yii::$app->request->getQueryParams();
        if (empty($GET_data['id'])  && empty($GET_data['user_id']) ) {
            return $response_data;
        }
        $friendModel = new Friend();
        $unfriend = $friendModel->unfriend($GET_data['user_id'], $GET_data['id']);
        if ($unfriend){
            $response_data = [
                'success' => true,
                'message' => Yii::t('app', 'ajax_unfriend_succefully'),
            ];
        }
        else {
            $response_data['message'] = Yii::t('app', 'ajax_unfriend_process_failed');
        }
        return $response_data;

    }

    public function saveStudentImage($base64img,$document){
        //$dir = Yii::$app->params['studentImagePath'];
        $dir = Yii::getAlias('@webroot').'/web/uploads/students/';
        $data = str_replace('data:image/jpg;base64,', '', $base64img);
        $data = str_replace(' ', '+', $data);
        $data = base64_decode($data);
        define('UPLOAD_DIR', $dir);
        $file = UPLOAD_DIR . $document.'.jpg';
        $success = file_put_contents($file, $data);
        $data = base64_decode($data);
        $source_img = imagecreatefromstring($data);
        $rotated_img = imagerotate($source_img, 0, 0);
        $imageSave = imagejpeg($rotated_img, $file, 10);
        imagedestroy($source_img);
    }



}
