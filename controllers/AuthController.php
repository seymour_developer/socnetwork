<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 07.04.2018
 * Time: 19:25
 */

namespace app\controllers;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\User;
use Yii;
use yii\web\Controller;

class AuthController extends Controller
{


    /*public function beforeAction(){

        if (Yii::$app->getUser()->isGuest){

            //return $this->redirect(['login']);
        }
    }*/


    public function actionSignup(){

        $model = new SignupForm();

        if(Yii::$app->request->isPost){

            $model->load(Yii::$app->request->post());
            if($model->signup()){
                return $this->redirect(['auth/login']);
            }
        }

        $this->layout = 'login';
        return $this->render('signup',['model' => $model]);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
            //return $this->redirect(['profile/about']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //return $this->goBack();
            return $this->redirect(['profile/index']);

        }

        $model->password = '';
        $this->layout = 'login.php';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        //return $this->goHome();
        return Yii::$app->response->redirect(['auth/login']);
    }


    public function actionTest(){
        $user = User::findOne(1);
        Yii::$app->user->login($user);

        if(Yii::$app->user->isGuest){
            die('you are a guest');
        }else {
            die('You are authorized');
        }
    }
}