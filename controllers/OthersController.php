<?php

namespace app\controllers;

use app\models\CityList;
use app\models\Friend;
use app\models\ProfessionalSkills;
use app\models\SchoolList;
use app\models\UniversityList;
use app\models\User;
use app\models\UserCityRel;
use app\models\UserPhotos;
use app\models\UserRelations;
use app\models\UserRelationshipStatus;
use app\models\UserSchoolRel;
use app\models\UserUniversityRel;
use app\models\UserWorkPlaces;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;

class OthersController extends Controller
{


   /* protected  function beforeAction()
    {
        if(Yii::app()->user->isGuest)
            Yii::app()->user->loginRequired ();
        return true;
    }*/
    /**
     * {@inheritdoc}
     */
    /*public $allowedPages = [
        "login", "error",
        // all about USERS
        'users', 'deleteuser', 'viewuser', 'updateuser',
        //"signup",
    ];*/

    /*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }*/


   /* public $allowedPages = [
        "login", "error",
        // all about USERS
        'users', 'deleteuser', 'viewuser', 'updateuser',
        //"signup",
    ];*/

    /*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => $this->allowedPages,
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }*/

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    /*public function actionIndex()
    {
        return $this->render('index');
    }*/


    /*public function actionIndex()
    {
        $request = Yii::$app->request;
        $user_id = Yii::$app->user->identity['id'];
        $userName = Yii::$app->request->url;
        $userPhotoModel = new UserPhotos();
        $friendModel = new Friend();
        $userModel = new User();
        $workPlaceModel = new UserWorkPlaces();
        $userUniversityModel = new UserUniversityRel();

        if (Yii::$app->request->isPost){
            if ($request->post('UserPhotos')) {
                $userPhotoModel->images['profile_image'] = UploadedFile::getInstances($userPhotoModel, 'profile_image');
                $userPhotoModel->images['cover_image'] = UploadedFile::getInstances($userPhotoModel, 'cover_image');
                $userPhotoModel->images['other_image'] = UploadedFile::getInstances($userPhotoModel, 'other_image');
                $userPhotoModel->uploadFile($user_id);
            }
        }

        /*if($request->post('chooseProfileImage')){
            $imageId = $request->post('chooseProfileImage');
            $userPhotoModel->setProfileImage($imageId,$user_id);
        }*/

        /*$current_profile_image = $userPhotoModel->getProfileImage($user_id);
        $current_cover_image = $userPhotoModel->getCoverImage($user_id);
        $user_photos = $userPhotoModel->getUserPhotos($user_id);
        $all_photos = $userPhotoModel->getAllPhotos($user_id);
        $friends = $friendModel->getAllFriends($user_id);
        $all_users = $userModel->allUsers($user_id);
        $currentWorkPlace = $workPlaceModel->getCurrentWorkPlace($user_id);
        $allWorkPlaces = $workPlaceModel->allWorkPlaces($user_id);
        $allUniversities = $userUniversityModel->getAllUserUniversities($user_id);*/

        /*return $this->render('index',
        ['userPhotoModel' => $userPhotoModel,
         'current_profile_image' => $current_profile_image,
         'current_cover_image' => $current_cover_image,
         'user_photos' => $user_photos,
         'all_photos' => $all_photos,
         'friends' => $friends,
         'all_users' => $all_users,
         'currentWorkPlace' => $currentWorkPlace,
         'allWorkPlaces' => $allWorkPlaces,
         'allUniversities' => $allUniversities
        ]);
    }*/


    /*public function actionIndex(){
        $request = Yii::$app->request;
        $user_id = $request->get('id');
        //$userName = Yii::$app->request->url;
        $userPhotoModel = new UserPhotos();
        $friendModel = new Friend();
        $userModel = new User();
        $workPlaceModel = new UserWorkPlaces();
        $userUniversityModel = new UserUniversityRel();

        if (Yii::$app->request->isPost){
            if ($request->post('UserPhotos')) {
                $userPhotoModel->images['profile_image'] = UploadedFile::getInstances($userPhotoModel, 'profile_image');
                $userPhotoModel->images['cover_image'] = UploadedFile::getInstances($userPhotoModel, 'cover_image');
                $userPhotoModel->images['other_image'] = UploadedFile::getInstances($userPhotoModel, 'other_image');
                $userPhotoModel->uploadFile($user_id);
            }
        }

        if($request->post('chooseProfileImage')){
            $imageId = $request->post('chooseProfileImage');
            $userPhotoModel->setProfileImage($imageId,$user_id);
        }

        $current_profile_image = $userPhotoModel->getProfileImage($user_id);
        $current_cover_image = $userPhotoModel->getCoverImage($user_id);
        $user_photos = $userPhotoModel->getUserPhotos($user_id);
        $all_photos = $userPhotoModel->getAllPhotos($user_id);
        $friends = $friendModel->getAllFriends($user_id);
        $all_users = $userModel->allUsers($user_id);
        $currentWorkPlace = $workPlaceModel->getCurrentWorkPlace($user_id);
        $allWorkPlaces = $workPlaceModel->allWorkPlaces($user_id);
        $allUniversities = $userUniversityModel->getAllUserUniversities($user_id);

        return $this->render('index',
        ['userPhotoModel' => $userPhotoModel,
         'current_profile_image' => $current_profile_image,
         'current_cover_image' => $current_cover_image,
         'user_photos' => $user_photos,
         'all_photos' => $all_photos,
         'friends' => $friends,
         'all_users' => $all_users,
         'currentWorkPlace' => $currentWorkPlace,
         'allWorkPlaces' => $allWorkPlaces,
         'allUniversities' => $allUniversities
        ]);
    }*/


    public function actionProfile(){
        $logged_user_id = Yii::$app->user->identity['id'];
        $request = Yii::$app->request;
        $user_id = $request->get('id');
        $userName = $request->get('name');
        if($user_id) {
            $userPhotoModel = new UserPhotos();
            $friendModel = new Friend();
            $userModel = new User();
            $workPlaceModel = new UserWorkPlaces();
            $userUniversityModel = new UserUniversityRel();
            $current_profile_image = $userPhotoModel->getProfileImage($user_id);
            $current_cover_image = $userPhotoModel->getCoverImage($user_id);
            $user_photos = $userPhotoModel->getUserPhotos($user_id);
            $all_photos = $userPhotoModel->getAllPhotos($user_id);
            $friends = $friendModel->getAllFriends($user_id);
            $all_users = $userModel->allUsers($user_id);
            $user_info = $userModel->getUserInfo($user_id);
            $friendship_status = $friendModel->getFriendshipStatusForUser($logged_user_id,$user_id);
            if ($request->post('send_friend_request')) {
                $request_sent = $friendModel->sendFriendRequest($logged_user_id,$user_id);
                if($request_sent){
                    return $this->redirect(['profile/profile','id' => $user_id]);
                }
            }
            $allWorkPlaces = $workPlaceModel->allWorkPlaces($user_id);
            $allUniversities = $userUniversityModel->getAllUserUniversities($user_id);
            /*if($request->post('friend-request-confirm')){
                $friend_request_id = $request->post('friend-request-confirm');
                var_dump($friend_request_id);
                die;
            }*/
            return $this->render('profile',
                ['userPhotoModel' => $userPhotoModel,
                    'current_profile_image' => $current_profile_image,
                    'current_cover_image' => $current_cover_image,
                    'user_photos' => $user_photos,
                    'all_photos' => $all_photos,
                    'friends' => $friends,
                    'all_users' => $all_users,
                    'user_info'  => $user_info,
                    'friendship_status' => $friendship_status,
                    'allWorkPlaces' => $allWorkPlaces,
                    'allUniversities' => $allUniversities
                ]);
        }

        else {

            return false;
        }
    }


    public function actionPhotoAlbums(){
        $request = Yii::$app->request;
        $user_id = $request->get('id');
        if($user_id){
            $userModel = new User();
            $user_info = $userModel->getUserInfo($user_id);
            $userPhotoModel = new UserPhotos();
            $current_profile_image = $userPhotoModel->getProfileImage($user_id);
            $current_cover_image = $userPhotoModel->getCoverImage($user_id);
            $user_photos = $userPhotoModel->getUserPhotos($user_id);
            $all_photos = $userPhotoModel->getAllPhotos($user_id);
        }
        else {

            echo 'error_page';
        }


        return $this->render('photo_albums',
            [
                'userPhotoModel' => $userPhotoModel,
                'current_profile_image' => $current_profile_image,
                'current_cover_image' => $current_cover_image,
                'user_photos' => $user_photos,
                'all_photos' => $all_photos,
                'user_info' => $user_info
            ]);
    }


    public function actionFriends(){
        $request = Yii::$app->request;
        $user_id = $request->get('id');
        $logged_user_id = Yii::$app->user->identity['id'];
        if($user_id) {
            $userPhotoModel = new UserPhotos();
            $friendModel = new Friend();
            $userModel = new User();
            /**
             * Upload new image
             */
            if (Yii::$app->request->isPost) {
                if ($request->post('UserPhotos')) {
                    $userPhotoModel->images['profile_image'] = UploadedFile::getInstances($userPhotoModel, 'profile_image');
                    $userPhotoModel->images['cover_image'] = UploadedFile::getInstances($userPhotoModel, 'cover_image');
                    $userPhotoModel->images['other_image'] = UploadedFile::getInstances($userPhotoModel, 'other_image');
                    $userPhotoModel->uploadFile($user_id);
                }
            }
            /**
             * Choose image from existing photos
             */
            if ($request->post('chooseProfileImage')) {
                $imageId = $request->post('chooseProfileImage');
                $userPhotoModel->setProfileImage($imageId, $user_id);
            }

            $current_profile_image = $userPhotoModel->getProfileImage($user_id);
            $current_cover_image = $userPhotoModel->getCoverImage($user_id);
            $user_photos = $userPhotoModel->getUserPhotos($user_id);
            $all_photos = $userPhotoModel->getAllPhotos($user_id);
            $friends = $friendModel->getAllFriends($user_id);
            $all_users = $userModel->allUsers($user_id);
            $user_info = $userModel->getUserInfo($user_id);
            $friendship_status = $friendModel->getFriendshipStatusForUser($logged_user_id,$user_id);
        }else {

            print 'error_page';
        }

        return $this->render('friends',
        [
            'userPhotoModel' => $userPhotoModel,
            'current_profile_image' => $current_profile_image,
            'current_cover_image' => $current_cover_image,
            'user_photos' => $user_photos,
            'all_photos' => $all_photos,
            'friends' => $friends,
            'all_users' => $all_users,
            'user_info' => $user_info,
            'friendship_status' => $friendship_status
        ]);
    }


    public function actionAbout(){
        $request = Yii::$app->request;
        $user_id = Yii::$app->user->identity['id'];
        $userPhotoModel = new UserPhotos();
        $friendModel = new Friend();
        $userModel = new User();
        $workPlaceModel = new UserWorkPlaces();
        $userUniversityModel = new UserUniversityRel();
        $universityListModel = new UniversityList();
        $profSkillModel = new ProfessionalSkills();
        $userSchoolModel = new UserSchoolRel();
        $schoolListModel = new SchoolList();
        $cityModel = new CityList();
        $userCityModel = new UserCityRel();
        $userRelationshipStatusModel = new UserRelationshipStatus();
        $userRelationModel  = new UserRelations();
        /**
         * Upload new image
         */
        if (Yii::$app->request->isPost){
            if ($request->post('UserPhotos')) {
                $userPhotoModel->images['profile_image'] = UploadedFile::getInstances($userPhotoModel, 'profile_image');
                $userPhotoModel->images['cover_image']   = UploadedFile::getInstances($userPhotoModel, 'cover_image');
                $userPhotoModel->images['other_image']   = UploadedFile::getInstances($userPhotoModel, 'other_image');
                $userPhotoModel->uploadFile($user_id);
            }
        }

        /**
         * Choose image from existing photos
         */
        if($request->post('chooseProfileImage')){
            $imageId = $request->post('chooseProfileImage');
            $userPhotoModel->setProfileImage($imageId,$user_id);
        }

        $current_profile_image = $userPhotoModel->getProfileImage($user_id);
        $current_cover_image = $userPhotoModel->getCoverImage($user_id);
        $user_photos = $userPhotoModel->getUserPhotos($user_id);
        $all_photos = $userPhotoModel->getAllPhotos($user_id);
        $friends = $friendModel->getAllFriends($user_id);
        $all_users = $userModel->allUsers($user_id);
        $schools = $schoolListModel->allSchoolList();
        $cities = $cityModel->getCityList();

        if ($workPlaceModel->load(Yii::$app->request->post()) && $workPlaceModel->validate()) {
            $company = Yii::$app->request->post("UserWorkPlaces")['company'];
            $position = Yii::$app->request->post("UserWorkPlaces")['position'];
            $city = Yii::$app->request->post("UserWorkPlaces")['city'];
            $start_date = Yii::$app->request->post("UserWorkPlaces")['start_date'];
            if(Yii::$app->request->post("UserWorkPlaces")['over_date']){
                $over_date =  Yii::$app->request->post("UserWorkPlaces")['over_date'];
            }

            if(Yii::$app->request->post('currently_work')){
                if(Yii::$app->request->post('currently_work') == 1){
                    $currently_work = Yii::$app->request->post('currently_work');
                }
            }

            $userWorkData = [
                'company' => $company,
                'position' => $position,
                'city' => $city,
                'start_date' => $start_date,
                'over_date' => $over_date,
                'currently_work' => $currently_work
            ];
            $workPlaceModel->saveUserWorkPlace($userWorkData);
            return $this->redirect(['/about']);
        }

        if ($userUniversityModel->load(Yii::$app->request->post()) && $userUniversityModel->validate()) {

            $university_code = $userUniversityModel->university_code;
            $user_id = Yii::$app->user->identity['id'];
            $study_start_date = $userUniversityModel->study_start_date;
            $study_over_date = $userUniversityModel->study_over_date;
            $is_graduated = $userUniversityModel->is_graduated;

            $userUniversity = [
                'university_code' => $university_code,
                'user_id' => $user_id,
                'study_start_date' => $study_start_date,
                'study_over_date' => $study_over_date,
                'is_graduated' => $is_graduated,
            ];

            $userUniversityModel->saveUniversityForUser($userUniversity);
            return $this->redirect(['/about']);
        }

        if(Yii::$app->request->isPost){
            if($request->post('skills') && !empty($request->post('skills'))){
                $created = $profSkillModel->saveUserSkills($user_id, $request->post('skills'));
                if($created){
                    return $this->redirect(['/about']);
                }
            }
        }

        if ($userSchoolModel->load(Yii::$app->request->post()) && $userSchoolModel->validate()) {

            $school_id = $userSchoolModel->school_id;
            $user_id = Yii::$app->user->identity['id'];
            $study_start_date = $userSchoolModel->study_start_date;
            $study_over_date = $userSchoolModel->study_over_date;
            $is_graduated = $userSchoolModel->is_graduated;
            $userSchoolData = [
                'school_id' => $school_id,
                'user_id' => $user_id,
                'study_start_date' => $study_start_date,
                'study_over_date' => $study_over_date,
                'is_graduated' => $is_graduated,
            ];

            $userSchoolModel->saveSchoolForUser($userSchoolData);
            return $this->redirect(['/about']);
        }

        if ($userCityModel->load(Yii::$app->request->post()) && $userCityModel->validate() ) {

            $userCityData = [];

            if(Yii::$app->request->post('current_city')){
                $city_id = $userCityModel->city_id;
                $user_id = Yii::$app->user->identity['id'];
                $userCityData = [
                    'city_id' => $city_id,
                    'user_id' => $user_id,
                    'is_current' => 1
                ];
            }

            if(Yii::$app->request->post('home_town')){
                $city_id = $userCityModel->city_id;
                $user_id = Yii::$app->user->identity['id'];
                $userCityData = [
                    'city_id' => $city_id,
                    'user_id' => $user_id,
                    'is_home_town' => 1
                ];
            }



            $userCityModel->addUserCity($userCityData);
            return $this->redirect(['/about']);
        }

        $allWorkPlaces = $workPlaceModel->allWorkPlaces($user_id);
        $universityList = $universityListModel->getUniversityList();
        $allUniversities = $userUniversityModel->getAllUserUniversities($user_id);
        $allSkills = $profSkillModel->getAllSKills($user_id);
        $allUserSchools = $userSchoolModel->getUserSchools($user_id);
        $currentCity = $userCityModel->getCurrentCity($user_id);
        $homeTown = $userCityModel->getHomeTown($user_id);
        $getCurRelStatus = $userRelationshipStatusModel->getRelationStatus($user_id);
        $getRelativesList = $userRelationModel->getUserRelations($user_id);
        $getRelativeTypes = $userRelationModel->getRelativeTypes();



        return $this->render('about',
            [
                'userPhotoModel' => $userPhotoModel,
                'current_profile_image' => $current_profile_image,
                'current_cover_image' => $current_cover_image,
                'user_photos' => $user_photos,
                'all_photos' => $all_photos,
                'friends' => $friends,
                'all_users' => $all_users,
                'workPlaceModel' => $workPlaceModel,
                'allWorkPlaces' => $allWorkPlaces,
                'userUniversityModel' => $userUniversityModel,
                'universityList' => $universityList,
                'allUniversities' => $allUniversities,
                'profSkillModel' => $profSkillModel,
                'allSkills' => $allSkills,
                'userSchoolModel' => $userSchoolModel,
                'schools' => $schools,
                'allUserSchools' => $allUserSchools,
                'cities' => $cities,
                'userCityModel' => $userCityModel,
                'currentCity' => $currentCity,
                'homeTown' => $homeTown,
                'getCurRelStatus' => $getCurRelStatus,
                'getRelativesList' => $getRelativesList,
                'getRelativeTypes' => $getRelativeTypes
            ]);
    }



    /* public function actionRemoveCity(){
        $request = Yii::$app->request;
        $id = $request->get('id');
        if($id){
            $userCityModel = new UserCityRel();
            $userCityModel->removeCity($id);
            return $this->redirect(['/about']);
        }
    }*/



    /*public function actionPhoto(){


    }*/

    /**
     * Login action.
     *
     * @return Response|string
     */


    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    /*public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }*/

    /**
     * Displays about page.
     *
     * @return string
     */
    /*public function actionAbout()
    {
        return $this->render('about');
    }*/
}
