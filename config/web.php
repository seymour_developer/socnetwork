<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'az',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
                ],
            ],
        ],

        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'MEX9rNOPmM30MfbZ7keOmkgU_8ck6HZ-',
            //'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['auth/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            // List all supported languages here
            // Make sure, you include your app's default language.
            'languages' => ['az','en','ru'],
            'enableLanguagePersistence' => false,
            'enableDefaultLanguageUrlCode' => true,
            //'on languageChanged' => 'app\components\User::onLanguageChanged',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
              /*  '<controller:\w+>/<action:\w+>/'=>'<controller>/<action>',*/
               /* ['pattern' => '',
                 'route' => 'auth/login'
                ],*/
                /*'<action:(profile|index|friends|about)>' => 'profile/<action>',*/
                '<a:(signup|login)>' => 'auth/<a>',
                '<user:\w+>/photo' => 'common/photo'
               /* '<prefix:\w+>/<controller:\w+>/<id:\d+>' => '<prefix><controller>/view',
                '<prefix:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<prefix><controller>/<action>',*/
/*                '<username>/<controller:(profile)>/<action:(index|profile|about)>' => '<controller>/<action>',*/
                /*'<user:\w+>/<id:\d+>' => 'user/view',*/
                /*'<user:\w+>/about' => 'profile/about',*/

                /*'<user:\w+>/friends' => 'profile/friends',*/

                /* '<user:\w+>/friends' => 'profile/friends',*/

                /*'<user:\w+>/friends' => 'profile/friends',*/
                /*'<user:(<user:/friends)>' => 'profile/<user>',*/
                /*'<user:\w+>/photo-albums' => 'profile/photo-albums',*/
                /*'<user:\w+>' => 'profile/index',*/
            ],
        ],

        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],

    ],
    'params' => $params,

    'as beforeRequest' => [
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [
                'allow' => true,
                'actions' => ['login','signup'],
            ],
            [
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
        'denyCallback' => function () {
            return Yii::$app->response->redirect(['auth/login']);
        },
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
